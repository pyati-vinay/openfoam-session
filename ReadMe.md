# OpenFOAM Session

A demo SMTK session for constructing an OpenFOAM model.

This session is a prototype only. It will allow you to:

1. set the working directory
2. set OpenFOAM's main controls
3. create a wind tunnel
4. add an obstacle (from auxiliary geometry) to a wind tunnel
5. annotate the model (boundary conditions, physical properties, etc.)

 These operations are expected to be performed in this order. If you are
 running natively with an Ubuntu build (see https://openfoam.org/download/),
 make sure you source OpenFOAM's environment prior to executing
 ModelBuilder ('source /opt/openfoam5/etc/bashrc'). If you are running on
 OS X with an OpenFOAM Docker container, make sure the OpenFOAM image is
 mounted prior to executing any operations
 ('openfoam-macos-file-system mount'). The exposed operations are designed
 to demonstrate the ability to integrate OpenFOAM into SMTK. It has been
 tested using the default values and OpenFOAM's provided 'motorBike.obj'
 obstacle.

NOTE: When running on OS X, there can be a memory corruption issue related
to loading in the python operations that results in one openfoam
python class being mistaken for another. Until this issue is resolved,
one workaround is to simply build the plugin with address sanitizer
enabled (`CMAKE_CXX_FLAGS=-fsanitize=address`,
`CMAKE_EXE_LINKER_FLAGS=-fsanitize=address`).
