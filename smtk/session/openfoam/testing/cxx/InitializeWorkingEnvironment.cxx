//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/PythonAutoInit.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/resource/Manager.h"

#include "smtk/session/openfoam/Registrar.h"
#include "smtk/session/openfoam/Resource.h"
#include "smtk/session/openfoam/Session.h"

#include "smtk/operation/Manager.h"
#include "smtk/operation/XMLOperation.h"

namespace
{
std::string scratch_dir = SMTK_OF_SCRATCH_DIR;
}

int InitializeWorkingEnvironment(int argc, char* argv[])
{
  // Create a resource manager
  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();

  // Register openfoam resources to the resource manager
  {
    smtk::session::openfoam::Registrar::registerTo(resourceManager);
  }

  // Create an operation manager
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();

  // Register openfoam operators to the operation manager
  {
    smtk::session::openfoam::Registrar::registerTo(operationManager);
  }

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  {
    std::string iweOpName =
      "openfoamsession.initialize_working_environment.initialize_working_environment";
    auto initializeWEOp = operationManager->create(iweOpName);
    if (!initializeWEOp)
    {
      std::cerr << "No initialize working environment operator\n";
      return 1;
    }

    initializeWEOp->parameters()
      ->findDirectory("working directory")
      ->setValue(scratch_dir += "/" + smtk::common::UUID::random().toString());

    auto initializeWEOpResult = initializeWEOp->operate();
    if (initializeWEOpResult->findInt("outcome")->value() !=
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "set working directory operator failed\n";
      std::cerr << initializeWEOpResult->findString("log")->value() << "\n";
      return 1;
    }

    smtk::session::openfoam::Resource::Ptr resource =
      smtk::dynamic_pointer_cast<smtk::session::openfoam::Resource>(
        initializeWEOpResult->findResource("resource")->value());

    resource->session()->removeWorkingDirectory();
  }

  return 0;
}
