//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/PythonAutoInit.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ModelEntityItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/operation/pybind11/PyOperation.h"

#include "smtk/session/openfoam/Registrar.h"
#include "smtk/session/openfoam/Resource.h"
#include "smtk/session/openfoam/Session.h"

#include "smtk/io/ExportMesh.h"

#include "smtk/mesh/core/Resource.h"

#include "smtk/model/EntityRef.h"
#include "smtk/model/Face.h"
#include "smtk/model/Group.h"
#include "smtk/model/Model.h"
#include "smtk/model/Resource.h"
#include "smtk/model/Tessellation.h"
#include "smtk/model/operators/AddAuxiliaryGeometry.h"

#include "smtk/operation/Manager.h"
#include "smtk/operation/XMLOperation.h"

#include "smtk/resource/Manager.h"

#include "smtk/extension/vtk/source/vtkModelMultiBlockSource.h"

#include "vtkActor.h"
#include "vtkCamera.h"
#include "vtkCommand.h"
#include "vtkCompositePolyDataMapper.h"
#include "vtkDataSetAttributes.h"
#include "vtkInteractorStyleSwitch.h"
#include "vtkNew.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkSmartPointer.h"
#include "vtkStringArray.h"
#include "vtkXMLMultiBlockDataWriter.h"

#include <fstream>

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

void cleanup(const std::string& file_path)
{
  //first verify the file exists
  ::boost::filesystem::path path(file_path);
  if (::boost::filesystem::is_directory(path))
  {
    std::cout << "is dir, about to remove" << std::endl;
    //remove the file_path if it exists.
    ::boost::filesystem::remove_all(path);
  }
  else
  {
    std::cout << "not a dir: " << file_path << std::endl;
  }
}

namespace
{
//SMTK_DATA_DIR is a define setup by cmake
std::string scratch_dir = SMTK_OF_SCRATCH_DIR;

std::string data_root = SMTK_OF_DATA_DIR;

void PrintFileToScreen(const std::string& filename)
{
  std::cout << "reading " << filename << std::endl;

  std::ifstream file;
  file.open(filename);

  //Fail check
  if (file.fail())
  {
    std::cout << "Could not read generated file." << std::endl;
  }

  while (file.good())
  {
    std::cout << (char)file.get();
  }
  std::cout << std::endl;
}

void UniqueEntities(const smtk::model::EntityRef& root, std::set<smtk::model::EntityRef>& unique)
{
  smtk::model::EntityRefArray children = (root.isModel()
      ? root.as<smtk::model::Model>().cellsAs<smtk::model::EntityRefArray>()
      : (root.isCellEntity()
            ? root.as<smtk::model::CellEntity>().boundingCellsAs<smtk::model::EntityRefArray>()
            : (root.isGroup() ? root.as<smtk::model::Group>().members<smtk::model::EntityRefArray>()
                              : smtk::model::EntityRefArray())));

  for (smtk::model::EntityRefArray::const_iterator it = children.begin(); it != children.end();
       ++it)
  {
    if (unique.find(*it) == unique.end())
    {
      unique.insert(*it);
      UniqueEntities(*it, unique);
    }
  }
}

void ParseModelTopology(smtk::model::Model& model, std::size_t* count)
{
  std::set<smtk::model::EntityRef> unique;
  UniqueEntities(model, unique);

  for (auto&& entity : unique)
  {
    if (entity.dimension() >= 0 && entity.dimension() <= 3)
    {
      count[entity.dimension()]++;
      float r = static_cast<float>(entity.dimension()) / 3;
      float b = static_cast<float>(1. - r);
      const_cast<smtk::model::EntityRef&>(entity).setColor(
        (r < 1. ? r : 1.), 0., (b < 1. ? b : 1.), 1.);
    }
  }
}

void VisualizeModel(smtk::model::Model& model)
{
  vtkNew<vtkActor> act;
  vtkNew<vtkModelMultiBlockSource> src;
  vtkNew<vtkCompositePolyDataMapper> map;
  vtkNew<vtkRenderer> ren;
  vtkNew<vtkRenderWindow> win;
  src->SetModelResource(model.resource());
  src->SetDefaultColor(1., 1., 0., 1.);
  map->SetInputConnection(src->GetOutputPort());
  act->SetMapper(map.GetPointer());
  act->GetProperty()->SetPointSize(5);
  act->GetProperty()->SetLineWidth(2);

  vtkNew<vtkCamera> camera;
  camera->SetPosition(-1., -1., -2.);
  camera->SetFocalPoint(0, 0, 0);

  ren->SetActiveCamera(camera.GetPointer());

  win->AddRenderer(ren.GetPointer());
  ren->AddActor(act.GetPointer());

  vtkRenderWindowInteractor* iac = win->MakeRenderWindowInteractor();
  vtkInteractorStyleSwitch::SafeDownCast(iac->GetInteractorStyle())
    ->SetCurrentStyleToTrackballCamera();
  win->SetInteractor(iac);

  win->Render();
  ren->ResetCamera();

  iac->Start();
}
}

int CreateWindTunnelOp(int argc, char* argv[])
{
  cleanup(scratch_dir + "/openfoamsession");

  // Create a resource manager
  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();

  // Register openfoam resources to the resource manager
  {
    smtk::session::openfoam::Registrar::registerTo(resourceManager);
  }

  // Create an operation manager
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();

  // Register openfoam operators to the operation manager
  {
    smtk::session::openfoam::Registrar::registerTo(operationManager);
  }

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  std::cout<<"Available operations:"<<std::endl;
  for (auto opName : operationManager->availableOperations())
  {
    std::cout<<opName<<std::endl;
  }
  std::cout<<std::endl;

  std::string workingDirectory = scratch_dir + "/openfoamsession";

  smtk::session::openfoam::Resource::Ptr resource;

  // Create openfoam resource and session
  {
    std::string iweOpName =
      "openfoamsession.initialize_working_environment.initialize_working_environment";
    auto initializeWEOp = operationManager->create(iweOpName);
    if (!initializeWEOp)
    {
      std::cerr << "No initialize working environment operator\n";
      return 1;
    }

    initializeWEOp->parameters()->findDirectory("working directory")->setValue(workingDirectory);

    auto initializeWEOpResult = initializeWEOp->operate();
    if (initializeWEOpResult->findInt("outcome")->value() !=
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Initialize working environment operator failed\n";
      std::cerr << initializeWEOpResult->findString("log")->value() << "\n";
      return 1;
    }

    resource = smtk::dynamic_pointer_cast<smtk::session::openfoam::Resource>(
      initializeWEOpResult->findResource("resource")->value());

    std::cout << "Initialize working environment operator succeeded\n";
  }

  {
    std::string smcOpName = "openfoamsession.set_main_controls.set_main_controls";
    auto smcOp = operationManager->create(smcOpName);
    std::cout<<"Just created "<<smcOp->typeName()<<std::endl;
    if (!smcOp)
    {
      std::cerr << "No set main controls operator\n";
      return 1;
    }
    smcOp->parameters()->findResource("resource")->setValue(resource);

    auto setMainControlsOpResult = smcOp->operate();
std::cout<<"setMainControls result: "<<setMainControlsOpResult->findInt("outcome")->value()<<std::endl;
    if (setMainControlsOpResult->findInt("outcome")->value() !=
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "set main controls operator failed\n";
      std::cerr << setMainControlsOpResult->findString("log")->value() << "\n";
      return 1;
    }

    PrintFileToScreen(workingDirectory + "/system/controlDict");
    std::cout << "Set main controls operator succeeded\n";
  }
  auto session = resource->session();

  smtk::model::Model windTunnel;
  {
    std::string smcOpName = "openfoamsession.create_wind_tunnel.create_wind_tunnel";
    auto cwtOp = operationManager->create(smcOpName);
    if (!cwtOp)
    {
      std::cerr << "No create wind tunnel operator\n";
      return 1;
    }
    cwtOp->parameters()->findResource("resource")->setValue(resource);

    std::array<double, 6> boundingBox = { -5, 15, -4, 4, 0, 8 };

    cwtOp->parameters()->findDouble("x dimensions")->setValue(0, boundingBox[0]);
    cwtOp->parameters()->findDouble("x dimensions")->setValue(1, boundingBox[1]);
    cwtOp->parameters()->findDouble("y dimensions")->setValue(0, boundingBox[2]);
    cwtOp->parameters()->findDouble("y dimensions")->setValue(1, boundingBox[3]);
    cwtOp->parameters()->findDouble("z dimensions")->setValue(0, boundingBox[4]);
    cwtOp->parameters()->findDouble("z dimensions")->setValue(1, boundingBox[5]);

    std::array<int, 3> numberOfCells = { 20, 8, 8 };

    cwtOp->parameters()
      ->findInt("number of cells")
      ->setValues(numberOfCells.begin(), numberOfCells.end());

    // 0: -X to +X
    int windDirectionIdx = 0;

    cwtOp->parameters()->findString("wind direction")->setDiscreteIndex(windDirectionIdx);

    auto createWindTunnelOpResult = cwtOp->operate();
    if (createWindTunnelOpResult->findInt("outcome")->value() !=
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "create wind tunnel operator failed\n";
      std::cerr << createWindTunnelOpResult->findString("log")->value() << "\n";
      return 1;
    }

    PrintFileToScreen(workingDirectory + "/system/blockMeshDict");

    windTunnel = createWindTunnelOpResult->findComponent("created")->valueAs<smtk::model::Entity>();
    if (!windTunnel.isValid())
    {
      std::cerr << "create wind tunnel operator constructed an invalid model\n";
      return 1;
    }

    std::size_t count[4] = { 0, 0, 0, 0 };
    ParseModelTopology(windTunnel, count);

    std::cout << "Wind tunnel:" << std::endl;
    std::cout << count[3] << " volumes" << std::endl;
    std::cout << count[2] << " faces" << std::endl;
    std::cout << count[1] << " edges" << std::endl;
    std::cout << count[0] << " vertex groups" << std::endl;

    // VisualizeModel(model);
    std::cout << "Create wind tunnel operator succeeded\n";
  }

  smtk::model::AuxiliaryGeometry obstacle;
  {
    // add auxiliary geometry
    auto addAuxOp = smtk::model::AddAuxiliaryGeometry::create();
    {
      std::string file_path(data_root);
      file_path += "/motorBike.obj";
      addAuxOp->parameters()->findFile("url")->setValue(file_path);
    }
    addAuxOp->parameters()->associateEntity(windTunnel);
    auto addAuxOpResult = addAuxOp->operate();
    if (addAuxOpResult->findInt("outcome")->value() !=
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Add auxiliary geometry failed!\n";
      return 1;
    }

    obstacle = addAuxOpResult->findComponent("created")->valueAs<smtk::model::Entity>();
    if (!obstacle.isValid())
    {
      std::cerr << "Auxiliary geometry is not valid!\n";
      return 1;
    }
  }

  {
    std::string aoOpName = "openfoamsession.add_obstacle.add_obstacle";
    auto aoOp = operationManager->create(aoOpName);
    if (!aoOp)
    {
      std::cerr << "No add obstacle operator\n";
      return 1;
    }

    aoOp->parameters()->findComponent("wind tunnel")->setValue(windTunnel.component());

    aoOp->parameters()->findComponent("obstacle")->setValue(obstacle.component());

    std::array<double, 6> boundingBox = { -1., 8., -.7, .7, 0., 2.5 };

    aoOp->parameters()->findDouble("x dimensions")->setValue(0, boundingBox[0]);
    aoOp->parameters()->findDouble("x dimensions")->setValue(1, boundingBox[1]);
    aoOp->parameters()->findDouble("y dimensions")->setValue(0, boundingBox[2]);
    aoOp->parameters()->findDouble("y dimensions")->setValue(1, boundingBox[3]);
    aoOp->parameters()->findDouble("z dimensions")->setValue(0, boundingBox[4]);
    aoOp->parameters()->findDouble("z dimensions")->setValue(1, boundingBox[5]);

    auto addObstacleOpResult = aoOp->operate();
    if (addObstacleOpResult->findInt("outcome")->value() !=
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "add obstacle operator failed\n";
      std::cerr << aoOp->log().convertToString() << "\n";
      return 1;
    }

    PrintFileToScreen(workingDirectory + "/system/snappyHexMeshDict");

    windTunnel = addObstacleOpResult->findComponent("created")->valueAs<smtk::model::Entity>();
    if (!windTunnel.isValid())
    {
      std::cerr << "add obstacle operator constructed an invalid model\n";
      return 1;
    }

    std::size_t count[4] = { 0, 0, 0, 0 };
    ParseModelTopology(windTunnel, count);

    std::cout << "Wind tunnel with obstacle:" << std::endl;
    std::cout << count[3] << " volumes" << std::endl;
    std::cout << count[2] << " faces" << std::endl;
    std::cout << count[1] << " edges" << std::endl;
    std::cout << count[0] << " vertex groups" << std::endl;

    // VisualizeModel(model);
  }

  // Clean up the directory for this session.
  if (session->workingDirectoryExists())
  {
   std::cout << "removing working directory " << workingDirectory << std::endl;
   session->removeWorkingDirectory();
  }

  return 0;
}
