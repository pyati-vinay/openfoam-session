//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_openfoam_Resource_h
#define pybind_smtk_session_openfoam_Resource_h

#include <pybind11/pybind11.h>

#include "smtk/model/Resource.h"
#include "smtk/session/openfoam/Resource.h"
#include "smtk/model/Resource.h"

namespace py = pybind11;

PySharedPtrClass< smtk::session::openfoam::Resource, smtk::model::Resource > pybind11_init_smtk_session_openfoam_Resource(py::module &m)
{
  PySharedPtrClass< smtk::session::openfoam::Resource, smtk::model::Resource > instance(m, "Resource");
  instance
    .def_static("create", (std::shared_ptr<smtk::session::openfoam::Resource> (*)()) &smtk::session::openfoam::Resource::create)
    .def_static("create", (std::shared_ptr<smtk::session::openfoam::Resource> (*)(::std::shared_ptr<smtk::session::openfoam::Resource> &)) &smtk::session::openfoam::Resource::create, py::arg("ref"))
    .def("session", &smtk::session::openfoam::Resource::session)
    .def("setSession", &smtk::session::openfoam::Resource::setSession)
    .def_static("CastTo", [](const std::shared_ptr<smtk::resource::Resource> i) {
        return std::dynamic_pointer_cast<smtk::session::openfoam::Resource>(i);
      })
    ;
  return instance;
}

#endif
