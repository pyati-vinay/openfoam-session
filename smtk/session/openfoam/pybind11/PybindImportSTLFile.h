//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_openfoam_ImportSTLFile_h
#define pybind_smtk_session_openfoam_ImportSTLFile_h

#include <pybind11/pybind11.h>

#include "smtk/session/openfoam/ImportSTLFile.h"

#include "smtk/mesh/core/Interface.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

namespace py = pybind11;

py::class_< smtk::session::openfoam::ImportSTLFile > pybind11_init_smtk_session_openfoam_ImportSTLFile(py::module &m)
{
  py::class_< smtk::session::openfoam::ImportSTLFile > instance(m, "ImportSTLFile");
  instance
    .def(py::init<>())
    .def("__call__", (smtk::mesh::ResourcePtr (smtk::session::openfoam::ImportSTLFile::*)(::std::string const &, const ::smtk::mesh::InterfacePtr &) const) &smtk::session::openfoam::ImportSTLFile::operator())
    .def("__call__", (bool (smtk::session::openfoam::ImportSTLFile::*)(::std::string const &, ::smtk::mesh::ResourcePtr) const) &smtk::session::openfoam::ImportSTLFile::operator())
    ;
  return instance;
}

#endif
