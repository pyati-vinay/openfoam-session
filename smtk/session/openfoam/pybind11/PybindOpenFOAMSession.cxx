//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/common/CompilerInformation.h"

SMTK_THIRDPARTY_PRE_INCLUDE
#include <pybind11/pybind11.h>
SMTK_THIRDPARTY_POST_INCLUDE

#include <utility>

namespace py = pybind11;

template <typename T, typename... Args>
using PySharedPtrClass = py::class_<T, std::shared_ptr<T>, Args...>;

#include "PybindSession.h"

#include "PybindImportSTLFile.h"
#include "PybindResource.h"
#include "PybindRegistrar.h"

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(_smtkPybindOpenFOAMSession, m)
{
  m.doc() = "<description>";

  py::module smtk = m.def_submodule("smtk", "<description>");
  py::module openfoam = smtk.def_submodule("openfoam", "<description>");

  // The order of these function calls is important! It was determined by
  // comparing the dependencies of each of the wrapped objects.
  PySharedPtrClass< smtk::session::openfoam::Resource, smtk::model::Resource > smtk_session_openfoam_Resource = pybind11_init_smtk_session_openfoam_Resource(openfoam);
  PySharedPtrClass< smtk::session::openfoam::Session, smtk::model::Session > smtk_session_openfoam_Session = pybind11_init_smtk_session_openfoam_Session(openfoam);
  py::class_< smtk::session::openfoam::ImportSTLFile > smtk_session_openfoam_ImportSTLFile = pybind11_init_smtk_session_openfoam_ImportSTLFile(openfoam);

  py::class_< smtk::session::openfoam::Registrar > smtk_session_openfoam_Registrar = pybind11_init_smtk_session_openfoam_Registrar(openfoam);
}
