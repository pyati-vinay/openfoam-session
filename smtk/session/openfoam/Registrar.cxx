//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/session/openfoam/Registrar.h"

#include "smtk/session/mesh/operators/Import.h"
#include "smtk/session/mesh/operators/Read.h"
#include "smtk/session/mesh/operators/Write.h"

#include "smtk/session/openfoam/Resource.h"

#include "smtk/operation/RegisterPythonOperations.h"

#include "smtk/operation/groups/CreatorGroup.h"
#include "smtk/operation/groups/ImporterGroup.h"
#include "smtk/operation/groups/ReaderGroup.h"
#include "smtk/operation/groups/WriterGroup.h"

namespace smtk
{
namespace session
{
namespace openfoam
{

namespace
{
typedef std::tuple<> OperationList;
}

void Registrar::registerTo(const smtk::resource::Manager::Ptr& resourceManager)
{
  resourceManager->registerResource<smtk::session::openfoam::Resource>();
}

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  // Register operations
  operationManager->registerOperations<OperationList>();

  smtk::operation::registerPythonOperations(
    operationManager, "openfoamsession.initialize_working_environment");

  smtk::operation::CreatorGroup(operationManager)
    .registerOperation<smtk::session::openfoam::Resource>(
      "openfoamsession.initialize_working_environment.initialize_working_environment");

  smtk::operation::registerPythonOperations(
    operationManager, "openfoamsession.add_obstacle");

  smtk::operation::registerPythonOperations(
    operationManager, "openfoamsession.create_wind_tunnel");

  smtk::operation::registerPythonOperations(
    operationManager, "openfoamsession.set_main_controls");
}

void Registrar::unregisterFrom(const smtk::resource::Manager::Ptr& resourceManager)
{
  resourceManager->unregisterResource<smtk::session::openfoam::Resource>();
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  operationManager->unregisterOperations<OperationList>();
}
}
}
}
