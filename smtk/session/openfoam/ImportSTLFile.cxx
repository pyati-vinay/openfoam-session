//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================

#include "smtk/session/openfoam/ImportSTLFile.h"
#include "smtk/extension/vtk/io/mesh/ImportVTKData.h"

#include "smtk/mesh/core/Interface.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "vtkCellData.h"
#include "vtkSTLReader.h"

namespace smtk
{
namespace session
{
namespace openfoam
{
ImportSTLFile::ImportSTLFile()
{
}

smtk::mesh::ResourcePtr ImportSTLFile::operator()(
  const std::string& filename, const smtk::mesh::InterfacePtr& interface) const
{
  smtk::mesh::ResourcePtr resource = smtk::mesh::Resource::create(interface);
  return this->operator()(filename, resource) ? resource : smtk::mesh::ResourcePtr();
}

bool ImportSTLFile::operator()(const std::string& filename, smtk::mesh::ResourcePtr resource) const
{
  vtkSmartPointer<vtkSTLReader> reader = vtkSmartPointer<vtkSTLReader>::New();
  reader->ScalarTagsOn();
  reader->SetFileName(filename.c_str());
  reader->Update();
  reader->GetOutput()->Register(reader);
  vtkDataSet* data = vtkDataSet::SafeDownCast(reader->GetOutput());

  smtk::extension::vtk::io::mesh::ImportVTKData importVTKData;
  if (data->GetCellData()->HasArray("STLSolidLabeling"))
  {
    return importVTKData(data, resource, "STLSolidLabeling");
  }
  else
  {
    return importVTKData(data, resource, "");
  }
}
}
}
}
