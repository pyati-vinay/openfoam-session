<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the OpenFOAM "initialize_working_environment" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operation -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="initialize working environment" Label="Session - Initialize Working Environment" BaseType="operation">
      <BriefDescription>
        Set working directory for creating/manipulating OpenFOAM models and initialize environment.
      </BriefDescription>
      <DetailedDescription>
        &lt;p&gt;Set working directory for creating/manipulating OpenFOAM models.
        &lt;p&gt;OpenFOAM's workflow operates around a top-level
        working directory. This operator sets that directory for
        subsequent OpenFOAM actions.
      </DetailedDescription>
      <ItemDefinitions>

        <Directory Name="working directory" Label="Working Directory" NumberOfRequiredValues="1">
          <BriefDescription>The directory in which the active OpenFOAM
          workflow resides.</BriefDescription>
        </Directory>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(initialize working environment)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::session::openfoam::Resource"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
