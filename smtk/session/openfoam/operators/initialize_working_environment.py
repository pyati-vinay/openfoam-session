#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

""" initialize_working_environment.py:

OpenFOAM's workflow operates around a top-level working directory. This operator
sets that directory for subsequent OpenFOAM actions and initialize the resource and seeion.
"""

import os

import openfoamsession
import openfoamsession.initialize_working_environment_xml

import smtk
import smtk.operation
import smtk.io.vtk


class initialize_working_environment(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "set working directory"

    def operateInternal(self):

        # Access the working directory
        workingDirectory = self.parameters().find(
            'working directory').value(0)

        resource = smtk.session.openfoam.Resource.create()
        session = smtk.session.openfoam.Session.create()
        resource.setSession(session)

        # Set the working directory in the session
        session.setWorkingDirectory(workingDirectory)

        # Exit with success
        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)

        created = result.findResource("resource")
        created.setValue(resource)

        return result

    def createSpecification(self):
        spec = smtk.attribute.Resource.create()
        reader = smtk.io.AttributeReader()
        reader.readContents(
            spec, openfoamsession.initialize_working_environment_xml.description, self.log())
        return spec
